# Parcel

Resolving dependency hell in [Parcel](https://parceljs.org/) projects.

## Installing

See https://parceljs.org/getting_started.html.

## Resolving

> Parcel makes this incredibly easy using [aliases](https://parceljs.org/module_resolution.html#aliases).

1. Edit `package.json` 

Simply add the `alias` field:

```json
{
  "main": "src/index.js",
  "alias": {
    "--": "./src"
  }
}
```
